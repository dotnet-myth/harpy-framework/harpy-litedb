﻿using Hangfire.LiteDB;
using System;

namespace Harpy.LiteDB.Presentation.Extensions {

    public static class HangfireLiteDB {

        public static LiteDbStorage UseLiteDb( string hangfireConnection = null ) {
            if ( string.IsNullOrEmpty( hangfireConnection ) )
                hangfireConnection = "Data Source=:memory:;";

            var options = new LiteDbStorageOptions {
                JobExpirationCheckInterval = TimeSpan.FromMinutes( 15 )
            };

            return new LiteDbStorage( hangfireConnection, options );
        }
    }
}