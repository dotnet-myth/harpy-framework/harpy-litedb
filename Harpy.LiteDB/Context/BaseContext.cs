﻿using LiteDB;

namespace Harpy.LiteDB.Context {

    public abstract class BaseContext {
        public readonly ILiteDatabase Database;
        public readonly BsonMapper Mapper;

        public BaseContext( ILiteDatabase database ) {
            Database = database;
            Mapper = BsonMapper.Global;
        }
    }
}