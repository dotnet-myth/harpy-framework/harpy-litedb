﻿using LiteDB;
using System;

namespace Harpy.LiteDB.IoC.Options {

    public class HarpyLiteDbOptions {
        public ILiteDatabase LiteDatabase { get; private set; }
        public Guid Guid { get; private set; }

        public HarpyLiteDbOptions( ILiteDatabase liteDatabase, Guid guid ) {
            LiteDatabase = liteDatabase;
            Guid = guid;
        }
    }
}