﻿using Harpy.IoC.Options;
using Harpy.LiteDB.Context;
using Harpy.LiteDB.IoC.Options;
using LiteDB;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.LiteDB.IoC {

    public static class InjectorContainer {
        private static IServiceCollection _services;

        public static ILiteDatabase UseLiteDbConnection( this BsonMapper mapper, string connectionString = default ) {
            if ( string.IsNullOrEmpty( connectionString ) )
                connectionString = "Data Source=:memory:";

            var database = new LiteDatabase( connectionString, mapper );
            return database;
        }

        public static void AddContext<TContext>( this HarpyLiteDbOptions contextOptions, Action<Parameters> contextParameter = null ) where TContext : BaseContext {
            _services.AddScoped( ( serviceProvider ) => {
                var args = new Parameters( );
                if ( contextParameter != null )
                    contextParameter.Invoke( args );

                var optionsList = serviceProvider.GetService<IEnumerable<HarpyLiteDbOptions>>( );

                var database = optionsList.FirstOrDefault( x => x.Guid == contextOptions.Guid );

                TContext context = ( TContext ) Activator.CreateInstance( typeof( TContext ), database.LiteDatabase, args );

                return context;
            } );
        }

        public static IServiceCollection AddDatabase( this IServiceCollection services, Func<BsonMapper, ILiteDatabase> UseConnection, Action<HarpyLiteDbOptions> AddContexts ) {
            _services = services;

            var mapper = BsonMapper.Global;

            var dbConnection = UseConnection( mapper );

            var optionsContext = new HarpyLiteDbOptions( dbConnection, Guid.NewGuid( ) );

            services.AddSingleton<HarpyLiteDbOptions>( ( serviceProvider ) => optionsContext );

            AddContexts.Invoke( optionsContext );

            _services = null;

            return services;
        }
    }
}