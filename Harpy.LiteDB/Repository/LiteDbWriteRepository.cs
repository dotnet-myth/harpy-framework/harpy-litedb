﻿using Harpy.LiteDB.Context;
using Harpy.LiteDB.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.LiteDB.Repository {

    public class LiteDbWriteRepository<T> : ILiteDbWriteRepository<T> where T : class {
        private readonly BaseContext _context;

        public LiteDbWriteRepository( BaseContext context ) {
            _context = context;
        }

        public virtual void Add( T entity ) =>
            _context.Database.GetCollection<T>( ).Insert( entity );

        public virtual void AddRange( IEnumerable<T> entities ) =>
             _context.Database.GetCollection<T>( ).Insert( entities );

        public virtual void Remove( T entity ) =>
            _context.Database.GetCollection<T>( ).DeleteMany( ent => ent == entity );

        public virtual void RemoveRange( IEnumerable<T> entities ) =>
            _context.Database.GetCollection<T>( ).DeleteMany( ent => entities.Contains( ent ) );

        public virtual void Update( T entity ) =>
            _context.Database.GetCollection<T>( ).Update( entity );

        public virtual void UpdateRange( IEnumerable<T> entities ) =>
            _context.Database.GetCollection<T>( ).Update( entities );
    }
}