﻿using Harpy.LiteDB.Context;
using Harpy.LiteDB.Domain.Interfaces.Repositories;
using LiteDB;
using Myth.Interfaces;
using Myth.Interfaces.Repositories.Results;
using Myth.Repositories.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.LiteDB.Repository {

    public class LiteDbReadRepository<T> : ILiteDbReadRepository<T> where T : class {
        private readonly BaseContext _context;

        public LiteDbReadRepository( BaseContext context ) {
            _context = context;
        }

        public virtual T FindById( BsonValue id ) =>
             _context.Database.GetCollection<T>( ).FindById( id );

        public virtual List<T> Search( ISpec<T> specification ) =>
            _context.Database.GetCollection<T>( ).Find( specification.Predicate ).ToList( );

        public virtual IPaginated<T> SearchPaginated( ISpec<T> specification ) {
            var filtered = _context.Database.GetCollection<T>( ).Find( specification.Predicate ).AsQueryable( );
            var all = _context.Database.GetCollection<T>( ).FindAll( ).AsQueryable( );
            var itens = specification.Prepare( filtered ).ToList( );
            var totalItens = specification.Filtered( all ).Count( );
            var pageSize = specification.ItensTaked;
            var pageNumber = ( specification.ItensSkiped > 0 ? ( specification.ItensSkiped / pageSize ) : 0 ) + 1;
            var totalPages = ( int ) Math.Ceiling( decimal.Divide( totalItens, ( pageSize > 0 ? pageSize : totalItens ) ) );
            var pagination = new Paginated<T>( pageNumber, pageSize, totalItens, totalPages, itens );

            return pagination;
        }

        public virtual int Count( ISpec<T> specification ) =>
            _context.Database.GetCollection<T>( ).Count( specification.Predicate );

        public virtual bool Any( ISpec<T> specification ) =>
             _context.Database.GetCollection<T>( ).Exists( specification.Predicate );

        public virtual T FirstOrDefault( ISpec<T> specification ) =>
             _context.Database.GetCollection<T>( ).FindOne( specification.Predicate );

        public virtual T LastOrDefault( ISpec<T> specification ) =>
            _context.Database.GetCollection<T>( ).Find( specification.Predicate ).LastOrDefault( );

        public virtual ILiteQueryable<T> AsQueryable( ) =>
           _context.Database.GetCollection<T>( ).Query();

        public virtual bool All( ISpec<T> specification ) =>
             !_context.Database.GetCollection<T>( ).Exists( specification.Predicate );

        public virtual List<T> ToList( ) =>
             _context.Database.GetCollection<T>( ).FindAll( ).ToList( );
    }
}