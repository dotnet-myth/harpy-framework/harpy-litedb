﻿using Harpy.LiteDB.Context;
using Harpy.LiteDB.Domain.Interfaces.Repositories;
using LiteDB;
using Myth.Interfaces;
using Myth.Interfaces.Repositories.Results;
using System.Collections.Generic;

namespace Harpy.LiteDB.Repository {

    public class LiteDbReadWriteRepository<T> : ILiteDbReadWriteRepository<T> where T : class {
        private readonly ILiteDbReadRepository<T> _readRepository;
        private readonly ILiteDbWriteRepository<T> _writeRepository;

        public LiteDbReadWriteRepository( BaseContext context ) {
            _readRepository = new LiteDbReadRepository<T>( context );
            _writeRepository = new LiteDbWriteRepository<T>( context );
        }

        public virtual void Add( T entity ) =>
            _writeRepository.Add( entity );

        public virtual void AddRange( IEnumerable<T> entities ) =>
            _writeRepository.AddRange( entities );

        public virtual bool All( ISpec<T> specification ) =>
            _readRepository.All( specification );

        public virtual bool Any( ISpec<T> specification ) =>
            _readRepository.Any( specification );

        public ILiteQueryable<T> AsQueryable( ) => 
            _readRepository.AsQueryable();

        public virtual int Count( ISpec<T> specification ) =>
            _readRepository.Count( specification );

        public virtual T FindById( BsonValue id ) =>
            _readRepository.FindById( id );

        public virtual T FirstOrDefault( ISpec<T> specification ) =>
            _readRepository.FirstOrDefault( specification );

        public virtual T LastOrDefault( ISpec<T> specification ) =>
            _readRepository.LastOrDefault( specification );

        public virtual void Remove( T entity ) =>
            _writeRepository.Remove( entity );

        public virtual void RemoveRange( IEnumerable<T> entities ) =>
            _writeRepository.RemoveRange( entities );

        public virtual List<T> Search( ISpec<T> specification ) =>
            _readRepository.Search( specification );

        public virtual IPaginated<T> SearchPaginated( ISpec<T> specification ) =>
            _readRepository.SearchPaginated( specification );

        public virtual List<T> ToList( ) =>
            _readRepository.ToList( );

        public virtual void Update( T entity ) =>
            _writeRepository.Update( entity );

        public virtual void UpdateRange( IEnumerable<T> entities ) =>
            _writeRepository.UpdateRange( entities );
    }
}