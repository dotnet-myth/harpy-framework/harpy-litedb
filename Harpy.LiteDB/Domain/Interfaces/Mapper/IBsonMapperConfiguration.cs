﻿using LiteDB;

namespace Harpy.LiteDB.Domain.Interfaces.Mapper {

    public interface IBsonMapperConfiguration<T> {

        void Configure( BsonMapper mapper );
    }
}