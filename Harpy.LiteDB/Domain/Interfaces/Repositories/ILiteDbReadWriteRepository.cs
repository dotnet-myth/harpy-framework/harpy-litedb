﻿namespace Harpy.LiteDB.Domain.Interfaces.Repositories {

    public interface ILiteDbReadWriteRepository<T> : ILiteDbReadRepository<T>, ILiteDbWriteRepository<T> {
    }
}