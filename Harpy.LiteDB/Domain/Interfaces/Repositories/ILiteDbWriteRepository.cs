﻿using Myth.Interfaces.Repositories.Base;
using System.Collections.Generic;

namespace Harpy.LiteDB.Domain.Interfaces.Repositories {

    public interface ILiteDbWriteRepository<T> : IRepository {

        void Add( T entity );

        void AddRange( IEnumerable<T> entity );

        void Update( T entity );

        void UpdateRange( IEnumerable<T> entities );

        void Remove( T entity );

        void RemoveRange( IEnumerable<T> entities );
    }
}