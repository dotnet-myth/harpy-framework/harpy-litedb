﻿using LiteDB;
using Myth.Interfaces;
using Myth.Interfaces.Repositories.Base;
using Myth.Interfaces.Repositories.Results;
using System.Collections.Generic;

namespace Harpy.LiteDB.Domain.Interfaces.Repositories {

    public interface ILiteDbReadRepository<T> : IRepository {

        T FindById( BsonValue id );

        bool All( ISpec<T> specification );

        bool Any( ISpec<T> specification );

        int Count( ISpec<T> specification );

        T FirstOrDefault( ISpec<T> specification );

        T LastOrDefault( ISpec<T> specification );

        List<T> Search( ISpec<T> specification );

        IPaginated<T> SearchPaginated( ISpec<T> specification );

        List<T> ToList( );
        ILiteQueryable<T> AsQueryable( );
    }
}